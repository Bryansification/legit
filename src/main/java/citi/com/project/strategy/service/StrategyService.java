package citi.com.project.strategy.service;

import citi.com.project.exceptions.StrategyNotFoundException;
import citi.com.project.strategy.entity.Strategy;

public interface StrategyService {

	public Strategy findById(int id) throws StrategyNotFoundException;
	public Strategy findByCode(String code) throws StrategyNotFoundException;
	public Strategy insertStrategy(String code, String name) throws StrategyNotFoundException;

	
}
