package citi.com.project.strategy.repo;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import citi.com.project.stocks.entity.Stock;
import citi.com.project.strategy.entity.Strategy;
import citi.com.project.strategy.entity.StrategyOrderStatus;
import citi.com.project.strategy.entity.StrategyOrders;

@Repository
public interface StrategyOrderRepository extends CrudRepository<StrategyOrders,Integer>{
	
	public Collection<StrategyOrders> findByStock(Stock stock);
	public Collection<StrategyOrders>  findByStrategy(Strategy strategy);
	public Optional<StrategyOrders> findById(int id);
	public Collection<StrategyOrders>  findByStatus(StrategyOrderStatus status);
	public Collection<StrategyOrders>  findByRequestAmnt(double RequestAmnt);
	public Collection<StrategyOrders>  findByStrategyAndStock(Strategy strategy, Stock stock);
	public Collection<StrategyOrders>  findByStrategyAndStatus(Strategy strategy, StrategyOrderStatus status);
}
