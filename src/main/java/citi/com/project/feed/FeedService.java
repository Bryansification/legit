package citi.com.project.feed;

import java.util.List;
import java.util.Map;

import citi.com.project.exceptions.PageNotFoundException;

public interface FeedService  {
	
	public Map<String, Double> getWatchList(List<String> watchList) throws PageNotFoundException;
	public List<Double> getSMAAverage(String stockSymbol) throws PageNotFoundException;
	public double getMarketPrice(String stockCode) throws PageNotFoundException;
	public List<Double> getMarketPriceMultiPast(String stockCode, int number) throws PageNotFoundException;
	public List<Map<String, String>> getSymbolList() throws PageNotFoundException;
	public List<Double> getBBSD(String stockSymbol) throws PageNotFoundException;
	public Map<String, Double> getOHLC(String stockCode, int periodInSecs) throws PageNotFoundException;
}
