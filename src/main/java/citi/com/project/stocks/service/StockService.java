package citi.com.project.stocks.service;

import java.util.Collection;
import java.util.Date;

import citi.com.project.exceptions.StockNotFoundException;
import citi.com.project.stocks.entity.Stock;
import citi.com.project.stocks.entity.StockFeedData;

public interface StockService {

	public Stock findById(int id) throws StockNotFoundException;

	public Stock findByStockCode(String code) throws StockNotFoundException;

	public Stock insertStock(String code, String name) throws StockNotFoundException;

	public Collection<Stock> findAllStock();

	// FeedService

	public Collection<StockFeedData> findAllFeedByStockCode(String stockCode) throws StockNotFoundException;

	public void insertStockFeedData(Double price, String code) throws StockNotFoundException;


}
