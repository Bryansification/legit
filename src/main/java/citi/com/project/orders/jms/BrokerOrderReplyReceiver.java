package citi.com.project.orders.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import citi.com.project.exceptions.OrderNotFoundException;
import citi.com.project.exceptions.StrategyOrderNotFoundException;
import citi.com.project.orders.entity.BrokerOrders;
import citi.com.project.orders.entity.OrderStatus;
import citi.com.project.orders.entity.Orders;
import citi.com.project.orders.service.OrderService;
import citi.com.project.strategy.implementation.StrategyImpl;

@Component
public class BrokerOrderReplyReceiver {
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	OrderService orderService;
	
	@Autowired
	StrategyImpl strategyImplService;
	
	private static final String source = "OrderBroker_Reply";
	private Logger logger = LoggerFactory.getLogger(BrokerOrderReplyReceiver.class);
	
	@JmsListener(destination=source,containerFactory="myFactory")
	public void receiveOrderReply (BrokerOrders orderReply) throws OrderNotFoundException, StrategyOrderNotFoundException {
		
		int retryCount = orderReply.getRetryCount();
		OrderStatus newStatus = orderReply.getStatus();
		int fillQuantity = orderReply.getFillQuantity();
		
		Orders orderToUpdate = orderService.findById(orderReply.getOrderId());

		//UPDATE EXISTING ORDER DETAILS
		orderToUpdate.setTryCount(retryCount);
		orderToUpdate.setStatus(newStatus);
		orderToUpdate.setFillQuantity(fillQuantity);
		
		logger.info("Received order reply at Main <" + orderReply.toString() + ">");
		logger.info("aaa" + orderReply);
		
		//TODO CALL A METHOD TO RETURN ORDERS OBJECT
		orderService.save(orderToUpdate);
		strategyImplService.receiveOrderReplyFromBroker(orderToUpdate);
	}
	
}
