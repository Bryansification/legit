package citi.com.project.orders.service;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import citi.com.project.exceptions.OrderNotFoundException;
import citi.com.project.orders.entity.OrderStatus;
import citi.com.project.orders.entity.Orders;
import citi.com.project.orders.repo.OrderRepository;
import citi.com.project.strategy.entity.StrategyOrders;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepository orderRepo;

	@Override
	public Orders insertOrder(Boolean buy, double price, int reqQuantity, StrategyOrders stratOrder) {
		Orders toInsert = new Orders(buy, price, reqQuantity, stratOrder);
		return orderRepo.save(toInsert);

	}

	@Override
	public Orders findById(int id) throws OrderNotFoundException {
		return orderRepo.findById(id).orElseThrow(() -> new OrderNotFoundException(id));

	}

	@Override
	public Collection<Orders> findByOrderStatus(OrderStatus status) {
		return orderRepo.findByStatus(status);

	}

	@Override
	public Collection<Orders> findByDateTime(Date startDate, Date endDate) {
		return orderRepo.findAllByOrderDateBetween(startDate, endDate);

	}

	@Override
	public Collection<Orders> findAllSell() {
		return orderRepo.findByBuy(false);
	}

	@Override
	public Collection<Orders> findAllBuy() {
		// TODO Auto-generated method stub
		return orderRepo.findByBuy(true);
	}

	@Override
	public Collection<Orders> findAllByStratOrderAndBuy(StrategyOrders stratOrder, Boolean buy) {
		// TODO Auto-generated method stub
		return orderRepo.findAllByStratOrderAndBuy(stratOrder, buy);
	}

	@Override
	public Orders save(Orders orders) {
		return orderRepo.save(orders);
	}

	@Override
	public Collection<Orders> findAllTransactionHistory() {
		return orderRepo.findAllNotPendingOrderByDateTime(OrderStatus.PENDING).stream()
				.filter(p -> (p.getStatus() == OrderStatus.FILLED) || (p.getStatus() == OrderStatus.REJECTED)
						|| (p.getStatus() == OrderStatus.PARTIALLY_FILLED && p.getTryCount() == 3))
				.collect(Collectors.toList());
	}

	@Override
	public Collection<Orders> findAllOrderList() {
		int currentDate = new Date().getDate();
		return orderRepo.findAllPastDay().stream().filter(p -> (p.getOrderDate().getDate() == currentDate))
				.collect(Collectors.toList());
	}

}
