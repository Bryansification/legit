package citi.com.project.exceptions;

public class StockNotFoundException extends Exception{
	public StockNotFoundException (int id) {
		super("Stock Id Not Found : " + id);
	}
	
	public StockNotFoundException (String code) {
		super("Stock Code Not Found : " + code);
	}
}
