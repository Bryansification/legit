import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';

import Navigation  from "./Navigation";
import StocksChart  from "./StocksChart";
import MarketData from "./MarketDataTable";
import Strategies from "./StrategiesTable";
import OrderBook from "./OrderBookTable";
import Position from "./PositionTable";
import TransactionHistory from "./TransactionHistoryTable";
import Watchlist from "./Watchlist"


const App = () => (
    <div>
        <Navigation />
        <MarketData />
        <Strategies />
        < OrderBook />
        < Position />
        < TransactionHistory />


        

    </div>
);

ReactDOM.render(<App/>, document.getElementById('root'));

