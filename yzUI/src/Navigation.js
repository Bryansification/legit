import React, {Component} from 'react';

import
{
  Alignment,
  Button,
  Classes,
  Navbar,
  NavbarDivider,
  NavbarGroup,
  NavbarHeading,
  Colors
} from "@blueprintjs/core";

import './App.css';


export default class Navigation extends React.Component{
  render(){
    return(
      <Navbar className={Classes.DARK}>
        <NavbarGroup align={Alignment.RIGHT} >
          <a id = "teamlogo" />
          <NavbarDivider />
          <Button className={Classes.MINIMAL} icon="dollar" text="Buy/Sell" />
          <Button className={Classes.MINIMAL} icon="book" text="Order Book" style={{marginLeft: 20}} />
          <Button className={Classes.MINIMAL} icon="reset" text="History" style={{marginLeft: 20}} />
        </NavbarGroup>
      </Navbar>
                
  )}
  
}