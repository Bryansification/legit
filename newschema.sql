
CREATE schema tradeplatform;

use tradeplatform;

CREATE TABLE stock (
 stock_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    stock_name VARCHAR(255) NOT NULL,
    stock_code VARCHAR(255) NOT NULL
);

CREATE TABLE strategy(
 strategy_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
     name VARCHAR(255) NOT NULL,
     code VARCHAR(255) NOT NULL
);

CREATE TABLE strategy_orders (
 strat_order_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    request_amnt DOUBLE NOT NULL,
    bb_period INT NOT NULL,
    pb_period INT NOT NULL,
    pb_trend INT NOT NULL,
    bbsd INT NOT NULL,
    stock_wallet INT NOT NULL,
    status INT,
    sma_indicator INT NOT NULL,
    moneywallet DOUBLE NOT NULL,
    stock_id INT NOT NULL,
    strategy_id INT NOT NULL,
    FOREIGN KEY stock_id(stock_id) REFERENCES stock(stock_id),
    FOREIGN KEY strategy_id(strategy_id) REFERENCES strategy(strategy_id)
);

# Additional Strategy Parameters

CREATE TABLE orders(
 order_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
      buy BIT,
      exits bit,
      fill_quantity INT NOT NULL,
      order_date DATETIME,
      price DOUBLE NOT NULL,
      req_quantity INT NOT NULL,
      status INT,
      try_count INT NOT NULL,
      strat_order_id INT NOT NULL,
      FOREIGN KEY strat_order_id(strat_order_id) REFERENCES strategy_orders(strat_order_id)
);
