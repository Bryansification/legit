import React, {component} from 'react';
import {Cell, Column, Table} from "@blueprintjs/table";

import
{
  Alignment,
  Button,
  Classes,
  H2,
  Colors, Intent,

} from "@blueprintjs/core";

import './App.css';

export default class OrderBookTable extends React.Component{

  
     render() { //Whenever our class runs, render method will be called automatically, it may have already defined in the constructor behind the scene.
      var rowIndex;
      const cellRenderer = (rowIndex) => {
        return <Cell></Cell>
      };
                 
        return (
        <div>
        <div>
        <H2 className = "headings" style ={{fontSize: 15, color: Colors.WHITE}}> {"Order Book"} </H2>
        </div>
        <div className = "tables">
          <Table numRows={5} className={Classes.DARK} style = {{margin: 20}}>
            <Column name="Symbol" cellRenderer={cellRenderer} />
            <Column name = "Date & Time"/>
            <Column name = "Side" />
            <Column name = "Price" />
            <Column name = "Quantity" />
            <Column name = "Status" />
          </Table>
          </div>
          </div>
        )}
     }
  