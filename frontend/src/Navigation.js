import React, {Component} from 'react';

import { Colors } from "@blueprintjs/core";

import
{
  Alignment,
  Button,
  Classes,
  Navbar,
  NavbarDivider,
  NavbarGroup,
  NavbarHeading,
} from "@blueprintjs/core";

import './App.css';

export interface NavigationProps {
}
export default class Navigation extends React.PureComponent<NavigationProps>{

  render(){

    return(
      <Navbar className={Classes.DARK} style={{height: 80}}>
        <NavbarGroup align={Alignment.LEFT} style={{marginTop: 10}}>
          <NavbarHeading id = "projectlogo"> hello</NavbarHeading>
        </NavbarGroup>
        <NavbarGroup align={Alignment.RIGHT}  style={{marginTop: 20}}>
          <Button className={Classes.MINIMAL} style={{marginRight: 20, textAlign:"center"}}> (walletlimit) <br/><br/>Wallet Limit</Button>
          <NavbarDivider />
          <Button className={Classes.MINIMAL} style={{marginLeft: 20, textAlign:"center"}}> (nettworth) <br/><br/>Net Worth</Button>
          <NavbarDivider />
          <Button className={Classes.MINIMAL} style={{marginLeft: 20, marginRight:30, textAlign:"center"}}> $ (pnl) <br/><br/>PnL</Button>
        </NavbarGroup>
      </Navbar>

  )}

}
