import React, { component } from 'react';
import { Cell, Column, Table, ColumnHeaderCell, EditableName, EditableCell, Regions } from "@blueprintjs/table";
import { ItemRenderer, MultiSelect, Select } from "@blueprintjs/select";
import BuySellOrder from "./BuySellOrder";
import axios from 'axios';
import * as Strategies from "./strategies";
import {
  Alignment,
  Button,
  Classes,
  H2,
  Icon,
  Intent,
  IconNames,
  button,
  Colors,
  MenuItem,
  ITagProps,
} from "@blueprintjs/core";

import './App.css';

import { ALIGN_RIGHT } from '@blueprintjs/icons/lib/esm/generated/iconContents';

export interface IStock {
  CompanyName: string;
  Symbol: string;
  SymbolID: number;
}

const StockMultiSelect = MultiSelect.ofType();
const INTENTS = [Intent.NONE, Intent.PRIMARY, Intent.SUCCESS, Intent.DANGER, Intent.WARNING];
const parseString = require('xml2js').parseString;
// const stockSelectProps = {
//   // itemPredicate: filterFilm,
//   itemRenderer: this.renderStock,
//   items: this.state.stocks,
// };
// export interface MultiSelectExampleState {
//     allowCreate: boolean;
//     createdItems: IStock[];
//     fill: boolean;
//     allStocks: IStock[];
//     hasInitialContent: boolean;
//     intent: boolean;
//     allStocks: IStock[];
//     openOnKeyDown: boolean;
//     popoverMinimal: boolean;
//     resetOnSelect: boolean;
//     tagMinimal: boolean;
// }

export default class MultiSelectExample extends React.Component {


  constructor() {
    super();
    this.state = {
      allStocks: [],
      strategy: Strategies.STRATEGIES[0],
      allowCreate: false,
      createdItems: [],
      stocks: [],
      fill: false,
      hasInitialContent: false,
      intent: false,
      items: [],
      openOnKeyDown: false,
      popoverMinimal: true,
      resetOnSelect: true,
      tagMinimal: false,
    }
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);
  }



  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.value != nextState.value) {
      return false;
    }
    return true;
  }


  componentWillMount() {
    fetch('http://feed2.conygre.com/API/StockFeed/GetSymbolList')
      //return response;
      .then((response) => response.text())
      .then((responseText) => {
        var self = this;
        parseString(responseText, function (err, result) {
          var stockJson = responseText;
          const stockObj = JSON.parse(stockJson);
          self.setState({ allStocks: stockObj });
        }.bind(this));
      })
      .catch((err) => {
        console.log('Error fetching the feed: ', err)
      });

  }

  render() {
    var rowIndex;
    var watchlist = null;
    const cellRenderer = (rowIndex) => {
      return <Cell></Cell>
    };

    const { allowCreate, stocks, hasInitialContent, tagMinimal, popoverMinimal, ...flags } = this.state;
    const getTagProps = (_value: string, index: number): ITagProps => ({
      intent: this.state.intent ? INTENTS[index % INTENTS.length] : Intent.NONE,
      minimal: tagMinimal,
    });

    const clearButton =
      stocks.length > 0 ? <Button icon="cross" minimal={true} onClick={this.handleClear} /> : undefined;
    let multiSelect;
    if (this.state.isOpen == true) {
      multiSelect =
        <div id="addwatchlist">
          <Button icon="minus" id="icon" onClick={this.handleClickClose}></Button>
          <StockMultiSelect
            itemRenderer={this.renderStock.bind(this)}
            items={this.state.allStocks}
            noResults={<MenuItem disabled={true} text="No results." />}
            onItemSelect={this.handleStockSelect}
            onItemsPaste={this.handleStocksPaste}
            popoverProps={{ minimal: popoverMinimal }}
            tagRenderer={this.renderTag}
            tagInputProps={{ tagProps: getTagProps, onRemove: this.handleTagRemove, rightElement: clearButton }}
            selectedItems={this.state.stocks}
          />

        </div>
    } else {
      multiSelect = <Button icon="add" id="icon" onClick={this.handleClickOpen}></Button>

    }

    return (
      <div>
        <div>
          <H2 className="headings" style={{ fontSize: 15, color: Colors.WHITE }}> {"Market Data"} </H2>

          <div style={{ display: "inline-block" }}>
            {multiSelect}
          </div>

        </div>
        <div className="tables">
          <Table numRows={5} className={Classes.DARK}>
            <Column name="Symbol" cellRenderer={cellRenderer} />
            <Column className="columns" name="Market Price" />
            <Column className="columns" name="Change" />
          </Table>
        </div>
      </div>
    );
  }

  renderTag = (stock: IStock) => stock.Symbol;

  // NOTE: not using Stocks.itemRenderer here so we can set icons.
  renderStock: ItemRenderer<IStock> = (stock, { modifiers, handleClick }) => {
    if (!modifiers.matchesPredicate) {
      return null;
    }
    return (
      <MenuItem
        active={modifiers.active}
        key={stock.SymbolID}
        label={stock.CompanyName}
        onClick={handleClick}
        text={`${stock.Symbol}`}
        shouldDismissPopover={false}
      />
  
    );
  };

  handleTagRemove = (_tag: string, index: number) => {
    this.deselectStock(index);
  };

  getSelectedStockIndex(stock: IStock) {
    return this.state.stocks.indexOf(stock);
  }

  isStockSelected(stock: IStock) {
    return this.getSelectedStockIndex(stock) !== -1;
  }

  selectStock(stock: IStock) {
    this.selectStocks([stock]);
  }

  selectStocks(stocksToSelect: IStock[]) {
    const { createdItems, stocks, allStocks } = this.state;

    let nextCreatedItems = createdItems.slice();
    let nextStocks = stocks.slice();
    let nextItems = allStocks.slice();

    stocksToSelect.forEach(stock => {
      const results = this.maybeAddCreatedStockToArrays(nextItems, nextCreatedItems, stock);
      nextItems = results.items;
      nextCreatedItems = results.createdItems;
      // Avoid re-creating an item that is already selected (the "Create
      // Item" option will be shown even if it matches an already selected
      // item).
      nextStocks = !this.arrayContainsStock(nextStocks, stock) ? [...nextStocks, stock] : nextStocks;
    });

    this.setState({
      createdItems: nextCreatedItems,
      stocks: nextStocks,
      items: nextItems,
    });
  }

  arrayContainsStock(stocks: IFilm[], stockToFind: IFilm): boolean {
    return stocks.some((stock: IFilm) => stock.Symbol === stockToFind.Symbol);
  }

  maybeAddCreatedStockToArrays(
    stocks: IStock[],
    createdItems: IStock[],
    stock: IStock,
  ): { createdItems: IStock[]; stocks: IStock[] } {
    const isNewlyCreatedItem = !this.arrayContainsStock(stocks, stock);
    return {
      createdItems: isNewlyCreatedItem ? this.addStockToArray(createdItems, stock) : createdItems,
      // Add a created stock to `stocks` so that the stock can be deselected.
      stocks: isNewlyCreatedItem ? this.addStockToArray(stocks, stock) : stocks,
    };
  }

  maybeDeleteCreatedStockFromArrays(
    stocks: IStock[],
    createdItems: IStock[],
    stock: IStock,
  ): { createdItems: IStock[]; stocks: IStock[] } {
    const wasItemCreatedByUser = this.arrayContainsStock(createdItems, stock);

    // Delete the item if the user manually created it.
    return {
      createdItems: wasItemCreatedByUser ? this.deleteStockFromArray(createdItems, stock) : createdItems,
      stocks: wasItemCreatedByUser ? this.deleteStockFromArray(stocks, stock) : stocks,
    };
  }

  addStockToArray(stocks: IFilm[], stockToAdd: IFilm) {
    return [...stocks, stockToAdd];
  }

  deleteStockFromArray(stocks: IFilm[], stockToDelete: IFilm) {
    return stocks.filter(stock => stock !== stockToDelete);
  }

  deselectStock(index: number) {
    const { stocks } = this.state;

    const stock = stocks[index];
    const { createdItems: nextCreatedItems, stocks: nextItems } = this.maybeDeleteCreatedStockFromArrays(
      this.state.items,
      this.state.createdItems,
      stock,
    );

    // Delete the item if the user manually created it.
    this.setState({
      createdItems: nextCreatedItems,
      stocks: stocks.filter((_stock, i) => i !== index),
      items: nextItems,
    });
  }
  

  handleStockSelect = (stock: IStock) => {
    if (!this.isStockSelected(stock)) {
      this.selectStock(stock);
    } else {
      this.deselectStock(this.getSelectedStockIndex(stock));
    }
  };

  handleStocksPaste = (stocks: IStock[]) => {
    // On paste, don't bother with deselecting already selected values, just
    // add the new ones.
    this.selectStocks(stocks);
  };

  handleClear = () => this.setState({ stocks: [] });


  handleClickOpen = () => {
    this.setState({ isOpen: true });
  }
  handleClickClose = () => {
    this.setState({ isOpen: false });
  }
}