import React, {component} from 'react';
import {Cell, Column, Table, ColumnHeaderCell, RowHeaderCell} from "@blueprintjs/table";

import
{
  Alignment,
  Button,
  Classes,
  H2,
  Icon,
  Intent,
  IconNames,
  button,
  Colors,
  MenuItem
} from "@blueprintjs/core";

import './App.css';
import { Color } from 'highcharts';

export default class Strategies extends React.Component{

  buttonsRenderer = () => {
    return (
      <Cell>
        <Button small = {true} icon = "play" id = "strategybutton1" intent = {Intent.SUCCESS}/>
        <Button small = {true} icon = "pause" id = "strategybutton2" intent = {Intent.WARNING}/>
      </Cell>
    );
  };
  render() { //Whenever our class runs, render method will be called automatically, it may have already defined in the constructor behind the scene.

        return (

          <div>
            <div>
            <H2 className="headings" style = {{fontSize:15, color: Colors.WHITE, marginTop: 20}} columnWidth = "auto">Strategies</H2>
            </div>
            <div className= "rowComponents">
              <Table numRows={10} defaultRowHeight= {30} defaultColumnWidth= {140} className={Classes.DARK} enableColumnResizing={false}>
                <Column name= "Strategy Name"/>
                <Column name = "Symbol"/>
                <Column name = "Money Invested" />
                <Column name = "Status" />
                <Column name = "PnL" />
                <Column name = "Net" />
                <Column name = "Actions" cellRenderer={this.buttonsRenderer}/>
              </Table>
            </div>
          </div>

        )}
     }
